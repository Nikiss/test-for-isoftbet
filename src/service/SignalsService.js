import { Signal } from "micro-signals";

const signals = {};

export function getSignal(name) {
    return signals[name] = signals[name] || new Signal();
}