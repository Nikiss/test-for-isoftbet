export class BaseCommand {
    constructor(model, view) {
        this.model = model;
        this.view = view;
        this.gameArea = view.gameArea;
        this.execute = () => { };
    }
}
