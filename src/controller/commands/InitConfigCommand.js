import { BaseCommand } from "./BaseCommand";
import { getSignal } from "../../service/SignalsService";

export class InitConfigCommand extends BaseCommand {
    constructor(model, view) {
        super(model, view);
        this.execute = () => {
            this.gameArea.view.interactive = true;
            this.gameArea.view.on('pointerdown', (data) => {
                model.addPoint = data.data.global;
                getSignal('addShape').dispatch();
            });
            document.getElementById('plus-persec-btn').addEventListener('click', () => model.setShapesPerSecond(1));
            document.getElementById('minus-persec-btn').addEventListener('click', () => model.setShapesPerSecond(-1));
            document.getElementById('plus-grav-btn').addEventListener('click', () => model.setGravityValue(1));
            document.getElementById('minus-grav-btn').addEventListener('click', () => model.setGravityValue(-1));
        };
    }
}
