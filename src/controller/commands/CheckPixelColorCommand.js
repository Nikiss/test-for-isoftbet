import { BaseCommand } from "./BaseCommand";
import { TweenMax } from "gsap";
import { Sprite, Rectangle } from "pixi.js";

export class CheckPixelColorCommand extends BaseCommand {
    constructor(model, view) {
        super(model, view)
        const rect = new Rectangle(view.gameArea.positionX, view.gameArea.positionY, view.gameArea.width, view.gameArea.height);
        const excludeColor1 = parseInt(view.gameArea.bgrColor.slice(2,4), 16);
        const excludeColor2 = parseInt(view.gameArea.bgrColor.slice(4,6), 16);
        const excludeColor3 = parseInt(view.gameArea.bgrColor.slice(6,8), 16);
        this.execute = () => {
            TweenMax.delayedCall(0.2, () => {
                const rt = view.app.renderer.generateTexture(view.gameArea.view, 1, 0, rect);
                const arr = view.app.renderer.extract.pixels(rt);
                const length = arr.length;
                let counter = 0;
                const s1 = new Date().getTime();
                for (let i = 0; i < length; i += 4) {
                    if (excludeColor1 != arr[i] || excludeColor2 != arr[i + 1] && excludeColor3 != arr[i + 2]) {
                        counter += 1;
                    }
                }
                console.log(new Date().getTime() - s1);
                model.activePixels = counter;
                this.execute();
            })
        };
    }
}


