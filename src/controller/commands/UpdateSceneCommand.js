import { BaseCommand } from "./BaseCommand";

export class UpdateSceneCommand extends BaseCommand {
    constructor(model, view) {
        super(model, view)
        this.execute = () => {
            document.getElementById('persectext').innerHTML = `Shapes per second: ${model.shapesPerSec().toString()}`;
            document.getElementById('gravtext').innerHTML = `Gravity Value: ${model.gravityValue().toString()}`;
            document.getElementById('numberCurShapes').innerHTML = `Number of current shapes ${model.activeShpes().toString()}`;
            document.getElementById('surfaceShapes').innerHTML =  `Area occupied by the shapes ${model.activePixels.toString()}`;
        };
    }
}
