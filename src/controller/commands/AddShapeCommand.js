import { BaseCommand } from "./BaseCommand";
import { TweenMax, Power0 } from "gsap";

export class AddShapeCommand extends BaseCommand {
    constructor(model, view) {
        super(model, view)
        this.execute = () => {
            const shapeType = model.shapeTypes[Math.round(Math.random() * (model.shapeTypes.length - 1))];
            const shape = view.gameArea.addShape(shapeType, null, model.addPoint);
            model.addPoint = undefined;
            shape.interactive = true;
            shape.buttonMode = true;
            model.shapes.push(shape);
            model.shapesAdded++;
            let remove = () => {
                model.shapes.splice(model.shapes.indexOf(shape), 1);
                view.gameArea.removeShape(shape)
            }
            shape.on('pointerdown', () => {
                tween.kill();
                remove();
            });
            var sec = model.gravityValue() * (model.shapeEndPoint - shape.y) / (model.shapeEndPoint - model.shapeStartPoint);
            let tween = TweenMax.to(shape, sec, { ease: Power0.easeNone, y: model.shapeEndPoint, onComplete: () => remove() });
        };
    }
}