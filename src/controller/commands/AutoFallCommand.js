import { BaseCommand } from "./BaseCommand";
import { TweenMax, Power0 } from "gsap";
import { getSignal } from "../../service/SignalsService";

export class AutoFallCommand extends BaseCommand {
    constructor(model, view) {
        super(model, view)
        this.execute = () => {
            TweenMax.delayedCall(1 / this.model.shapesPerSec(), () => {
                getSignal('addShape').dispatch();
                this.execute();
            })
        };
    }
}
