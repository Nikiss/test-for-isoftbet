import { BaseCommand } from "./commands/BaseCommand";

export class GameController {
    constructor(context) {
        let mapCommands = new Map();
        this.registerCommand = (name, command) => {
            if (mapCommands.get(name) !== undefined) {
                console.log(`command ${name} registered`);
            } else {
                mapCommands.set(name, command);
                console.log(mapCommands);
            }
        }

        this.removeCommand = (name) => {
            mapCommands.delete(name);
        }

        this.executeCommand = (name) => {
            if (mapCommands.get(name) == undefined) {
                console.log(`command ${name} is not registered`);
            }
            else {
                let cmd = mapCommands.get(name);
                new cmd(context.model, context.view).execute();
            }
        }
    }
}
