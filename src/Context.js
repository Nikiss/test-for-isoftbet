import { GameController } from "./controller/GameController";
import { GameModel } from "./model/GameModel";
import { GameView } from "./view/GameView";
import { AutoFallCommand } from "./controller/commands/AutoFallCommand";
import { getSignal } from "./service/SignalsService";
import { AddShapeCommand } from "./controller/commands/AddShapeCommand";
import { InitConfigCommand } from "./controller/commands/InitConfigCommand";
import { Ticker } from "pixi.js";
import { UpdateSceneCommand } from "./controller/commands/UpdateSceneCommand";
import { CheckPixelColorCommand } from "./controller/commands/CheckPixelColorCommand";

export class Context {
    constructor(width, height) {
        const controller = new GameController(this);
        const model = new GameModel(width, height);
        const view = new GameView(width, height);
        const ticker = new Ticker();

        controller.registerCommand('addShape', AddShapeCommand);
        controller.registerCommand('autoFall', AutoFallCommand);
        controller.registerCommand('init', InitConfigCommand);
        controller.registerCommand('checkColor', CheckPixelColorCommand);
        controller.registerCommand('updateScene', UpdateSceneCommand);

        getSignal('addShape').add(() => controller.executeCommand('addShape'));
        getSignal('start').add((x, y) => {
            controller.executeCommand('autoFall');
            controller.executeCommand('init');
            controller.executeCommand('checkColor');
        });
        ticker.add(() => controller.executeCommand('updateScene'), this);

        ticker.start();
        this.controller = controller;
        this.model = model;
        this.view = view;
    }
}