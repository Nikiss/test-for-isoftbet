import { Container, Graphics } from "pixi.js";
import { Shape } from "./Shape";

export class GameArea {
    constructor(width, height) {
        this.positionX = 100;
        this.bgrColor = '0x7b7d7a';
        this.positionY = 100;
        const view = new Container();

        const bgr = new Graphics();
        bgr.beginFill(parseInt(this.bgrColor, 16));
        bgr.drawRect(this.positionX, this.positionY, width, height);
        bgr.endFill();
        view.addChild(bgr);

        const mask = new Graphics();
        mask.beginFill(0x7b7d00);
        mask.drawRect(this.positionX, this.positionY, width, height);
        mask.endFill();
        view.addChild(mask);
        view.mask = mask;

        this.addShape = (type, color, point) => {
            const shape = new Shape(type, color ? color : getRandomColor());
            if (point != undefined) {
                shape.view.x = point.x;
                shape.view.y = point.y;
            } else {
                shape.view.x = Math.random() * (width - shape.view.width) + this.positionX;
                shape.view.y = this.positionY - shape.view.height;
            }
            view.addChild(shape.view);
            return shape.view;
        }

        let getRandomColor = () => {
            var letters = '0123456789ABCDEF';
            var color = '0x';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            if (color === this.bgrColor) {
                getRandomColor();
            }
            return color;
        }

        this.removeShape = (shape) => view.removeChild(shape);
        this.width = width;
        this.height = height;
        this.view = view;
    }
}
