import { Graphics } from "pixi.js";

export class Shape {
    constructor(type, color) {
        const view = new Graphics();
        view.beginFill(color ? color : 0x000000);
        const rnd = !Math.round(Math.random());
        switch (type) {
            case 'triangle':
                view.moveTo(0, 50);
                view.lineTo(25, 0);
                view.lineTo(50, 50);
                view.lineTo(0, 50);
                view.closePath();
                break;
            case 'rectangle1':
                view.drawRect(0, 0, 50, 50);
                break;
            case 'rectangle2':
                view.drawRect(0, 0, 50 + (rnd ? 1 : 0) * 50, 50 + (rnd ? 0 : 1) * 50);
                break;
            case 'pentagon':
                view.moveTo(10, 50);
                view.lineTo(0, 20);
                view.lineTo(25, 0);
                view.lineTo(50, 20);
                view.lineTo(40, 50);
                view.lineTo(10, 50);
                view.closePath();
                break;
            case 'hexagon':
                view.moveTo(12.5, 50);
                view.lineTo(0, 25);
                view.lineTo(12.5, 0);
                view.lineTo(37.5, 0);
                view.lineTo(50, 25);
                view.lineTo(37.5, 50);
                view.lineTo(12.5, 50);
                view.closePath();
                break;
            case 'circle':
                view.drawCircle(25, 25, 25);
                break;
            case 'ellipse':
                if (rnd) {
                    view.drawEllipse(25, 12.5, 50, 25);
                }
                else {
                    view.drawEllipse(12.5, 25, 25, 50);
                }
                break;
            case 'star':
                view.drawStar(0, 0, 6, 25);
                break;
            case 'random':
                const random = Math.round(Math.random() * 3 + 3);
                let path= [];
                for (let i = 0; i < random * 2; i++) {
                    path.push(Math.round(Math.random() * 50));
                }
                view.drawPolygon(path);
                break;
        }
        view.endFill();
        this.view = view;
    }
}