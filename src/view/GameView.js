import { Application } from "pixi.js";
import { GameArea } from "./elements/GameArea";

export class GameView {
    constructor(width, height) {
        const gamewidth = 500;
        const gameheight = 500;
        const backgroundColor = 0xbdc1c7;
        
        this._width = width;
        this._height = height;

        const app = new Application({ width, height, antialias: true, autoDensity: true, resolution: devicePixelRatio || 1, backgroundColor });
        document.getElementById('game').appendChild(app.view);

        const game = new GameArea(gamewidth, gameheight);
        app.stage.addChild(game.view);

        this.gameArea = game;
        this.app = app;
    }
}
