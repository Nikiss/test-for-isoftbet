export class GameModel {
    constructor(width, height) {
        this.height = height;
        this.width = width;
        this.shapeStartPoint = 50;
        this.shapeEndPoint = 600;
        let gravityValueRange = [1, 2, 5, 10];
        let gravityValue = 1;
        let shapesPerSec = 1;
        let shapesPerSecRange = [1, 2, 3, 5, 10, 15, 25];
        this.activePixels = 0;
        this.shapes = [];
        this.shapesAdded = 0;
        this.addPoint = undefined;
        this.shapeTypes = ['triangle', 'rectangle1', 'rectangle2', 'pentagon', 'hexagon', 'circle', 'ellipse', 'random'];

        this.setGravityValue = (v) => {
            gravityValue = gravityValue + v < 0 ? 0 : gravityValue + v > gravityValueRange.length - 1 ? gravityValueRange.length - 1 : gravityValue + v;
            return gravityValueRange[gravityValue];
        }
        this.setShapesPerSecond = (v) => {
            shapesPerSec = shapesPerSec + v < 0 ? 0 : shapesPerSec + v > shapesPerSecRange.length - 1 ? shapesPerSecRange.length - 1 : shapesPerSec + v;
            return shapesPerSecRange[shapesPerSec];
        }
        this.shapesPerSec = () => shapesPerSecRange[shapesPerSec];
        this.gravityValue = () => gravityValueRange[gravityValue];
        this.activeShpes = () => this.shapes.length;
    }
}
