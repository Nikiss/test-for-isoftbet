const HtmlWebpackPlugin = require('html-webpack-plugin');
const config = {
    entry: './src/main.js',
    plugins: [
        new HtmlWebpackPlugin({template: 'assets/index.html'}),
    ],
    devServer: {
        compress: true,
        port: 9000
    }
}

module.exports = (env, argv) => {
    if (argv.mode === 'development') {
        config.devtool = 'source-map';
    }
    return config;
};
